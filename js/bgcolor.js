﻿var b1 = document.getElementById("changeBgButton1");
var b2 = document.getElementById("changeBgButton2");
var b = document.getElementById("changeBg");
var b3 = document.getElementById("RemoveColor")

b1.addEventListener("click", changeBg1);
b2.addEventListener("click", changeBg2);
b.addEventListener("input", changeBg);
b3.addEventListener("click",RemoveBg);

function changeBg1() {
    document.body.style.backgroundColor = "red";
}

function changeBg2() {
    document.body.style.backgroundColor = "green";
}

function changeBg() {
    console.log("Background changed to : " + b.value);
    document.body.style.backgroundColor = b.value;
}
function RemoveBg() {
    document.body.style.backgroundColor = "grey";
}